package hust.soict.ictglobal.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class AWTCounter extends Frame implements ActionListener, WindowListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount; // Declare a Button component
	private int count = 0; // Counter's value
	// Constructor to setup GUI components and event handlers

	public AWTCounter() {
		setLayout(new FlowLayout());
		// "super" Frame, which is a Container, sets its layout to FlowLayout to arrange
		// the components from left-to-right, and flow to next row from top-to-bottom.
		lblCount = new Label("Counter"); // construct the Label component

		add(lblCount); // "super" Frame container adds Label component
		tfCount = new TextField(count + "", 20); // construct the TextField component with initial text
		tfCount.setEditable(false); // set to read-only
		add(tfCount); // "super" Frame container adds TextField component
		btnCount = new Button("Count"); // construct the Button component
		add(btnCount); // "super" Frame container adds Button component
		btnCount.addActionListener(this);

		setTitle("AWT Counter"); // "super" Frame sets its title
		setSize(400, 120); // "super" Frame sets its initial window size

		setVisible(true); // "super" Frame shows
		System.out.println(this);
		System.out.println(lblCount);
		System.out.println(tfCount);
		System.out.println(btnCount);

		addWindowListener(this);
		setLocation(500, 400);
	}

	public static void main(String[] args) {
		new AWTCounter();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		++count; // Increase the counter value
		tfCount.setText(count + ""); // Convert int to String
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
	}
}
