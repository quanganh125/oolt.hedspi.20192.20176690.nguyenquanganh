package hust.soict.ictglobal.gui.swing;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class SwingDemo extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructor
	public SwingDemo() {
		// Get the content-pane of this JFrame, which is a java.awt.Container
		// All operations, such as setLayout() and add() operate on the content-pane
		Container cp = getContentPane();
		cp.add(new JLabel("Hello, world!"));
		cp.add(new JButton("Button"));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Set the content-pane of this JFrame to the main JPanel
		setTitle("Hello World"); // "super" JFrame sets title
		setSize(400, 120);
		setLocation(500,400);
		setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new SwingDemo();
			}
			
		});
	}
}
