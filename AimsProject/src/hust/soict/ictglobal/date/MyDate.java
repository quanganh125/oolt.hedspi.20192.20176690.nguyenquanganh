package hust.soict.ictglobal.date;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MyDate {
	private int day;
	private int month;
	private int year;
	private String date;

	public int getDay() {
		return day;
	}

	public boolean setDay(int day) {
		switch (this.month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: {
			if (day < 1 || day > 30)
				return false;
			this.day = day;
			return true;
		}
		case 4:
		case 6:
		case 9:
		case 11: {
			if (day < 1 || day > 31)
				return false;
			this.day = day;
			return true;
		}
		case 2: {
			if ((day < 1 || day > 29) && this.month % 4 != 0)
				return false;
			if ((day < 1 || day > 28) && this.month % 4 == 0)
				return false;
			this.day = day;
			return true;
		}
		default:
			return false;
		}
	}

	public int getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		switch (month) {
		case "January":
			this.month = 1;
			break;
		case "February":
			this.month = 2;
			break;
		case "March":
			this.month = 3;
			break;
		case "April":
			this.month = 4;
			break;
		case "May":
			this.month = 5;
			break;
		case "June":
			this.month = 6;
			break;
		case "July":
			this.month = 7;
			break;
		case "August":
			this.month = 8;
			break;
		case "September":
			this.month = 9;
			break;
		case "October":
			this.month = 10;
			break;
		case "November":
			this.month = 11;
			break;
		case "December":
			this.month = 12;
			break;
		default:
			System.out.println("False");
			break;
		}
	}

	public boolean setMonth(int month) {
		if (month < 1 || month > 12)
			return false;
		this.month = month;
		return true;
	}

	public int getYear() {
		return year;
	}

	public boolean setYear(int year) {
		if (year < 0)
			return false;
		this.year = year;
		return true;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public void printSpeFormat() {
		System.out.print("Specified Format: ");
		System.out.println(this.day+"/"+this.month+"/"+this.year);
	}

	public void print() {
		System.out.print("String version: ");
		switch (this.month) {
		case 1:
			System.out.print("January ");
			break;
		case 2:
			System.out.print("February ");
			break;
		case 3:
			System.out.print("March ");
			break;
		case 4:
			System.out.print("April ");
			break;
		case 5:
			System.out.print("May ");
			break;
		case 6:
			System.out.print("June ");
			break;
		case 7:
			System.out.print("July ");
			break;
		case 8:
			System.out.print("August ");
			break;
		case 9:
			System.out.print("September ");
			break;
		case 10:
			System.out.print("October ");
			break;
		case 11:
			System.out.print("November ");
			break;
		default:
			System.out.print("December ");
			break;
		}
		System.out.print(this.day);
		if (this.day == 1 || this.day == 21 || this.day == 31)
			System.out.print("st ");
		else if(this.day == 2 || this.day == 22)
			System.out.print("nd ");
		else if(this.day == 3 || this.day == 23)
			System.out.print("rd ");
		else System.out.print("th ");
		System.out.println(this.year);
	}

	public void accept(String date) {
		String myString = new String();
		int i, j;
		String strDay, strMonth, strYear;
		myString = date;
		for (i = 0; i < myString.length(); i++)
			if (myString.charAt(i) == ' ')
				break;
		for (j = i + 1; j < myString.length(); j++)
			if (myString.charAt(j) == ' ')
				break;

		strDay = myString.substring(0, i);
		strMonth = myString.substring(i + 1, j);
		strYear = myString.substring(j + 1, myString.length());

		this.setYear(Integer.parseInt(strYear));

		int month;
		try {
			month = Integer.parseInt(strMonth);
		} catch (NumberFormatException e) {
			month = 0;
		}

		if (month == 0)
			this.setMonth(strMonth);
		else
			this.setMonth(Integer.parseInt(strMonth));

		this.setDay(Integer.parseInt(strDay));
		this.setDate(date);
	}
	
	public void setCurrentDate() {
		Calendar cal = Calendar.getInstance();
		this.year = cal.get(Calendar.YEAR);
		this.month = cal.get(Calendar.MONTH+1);
		this.day = cal.get(Calendar.DAY_OF_MONTH);
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.date = sdf.format(date);
		//System.out.println(this.date);
	}
}
