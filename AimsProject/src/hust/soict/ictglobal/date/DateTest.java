package hust.soict.ictglobal.date;

import java.util.Scanner;

public class DateTest {
	public static void main(String[] args) {	
		Scanner scan = new Scanner(System.in);
		String date;	
		
		MyDate mydate1 = new MyDate();	
		System.out.print("Current date 1: ");	
		date = scan.nextLine();
		mydate1.accept(date);
		
		MyDate mydate2 = new MyDate();
		System.out.print("Current date 2: ");	
		date = scan.nextLine();
		mydate2.accept(date);
		
		mydate1.print();
		
		System.out.println(DateUtils.compareTwoDates(mydate1, mydate2));
		DateUtils.sortDates(mydate1, mydate2);
		System.out.println("After sort:(latest date -> oldest date)*****************************");
		System.out.print("Current date 1: ");	
		System.out.println(mydate1.getDate());
		System.out.print("Current date 2: ");	
		System.out.println(mydate2.getDate());
		scan.close();
	}
}