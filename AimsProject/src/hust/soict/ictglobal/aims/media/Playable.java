package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

interface Playable {
	
	public void play() throws PlayerException;
}
