package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<Track>();

	@Override
	public int compareTo(CompactDisc obj) {
		// SORT BY NUMBER OF TRACKS. IF EQUAL -> SORT BY LENGTH

		if (this.tracks.size() > obj.tracks.size())
			return 1;
		else if (this.tracks.size() < obj.tracks.size())
			return -1;
		else {
			return this.getLength() >= obj.getLength() ? 1 : -1;
		}
	}

	@Override
	public String toString() {
		return "CD's name= " + this.getTitle() + "\tCategory= " + this.getCategory() + "\tCost= " + this.getCost();
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void addTrack(Track obj) {
		if (this.tracks.contains(obj))
			System.out.println("Track already exist!");
		else {
			this.tracks.add(obj);
			System.out.println("Add track sucessfully!");
		}
	}

	public void removeTrack(Track obj) {
		if (this.tracks.contains(obj)) {
			this.tracks.remove(obj);
			System.out.println("Remove track sucessfully!");
		} else
			System.out.println("Track does not exist!");
	}

	public int getLength() {
		return this.length + super.getLength();
	}

	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERR: CD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD Length : " + this.getLength());
		System.out.println("Track: ");

		Iterator<Track> iter = tracks.iterator();
		Track nexTrack;
		while(iter.hasNext()) {
			nexTrack = (Track)iter.next();
			try {
				nexTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
	}

	public CompactDisc() {
	}
}
