package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class Track implements Playable,Comparable<Track> {
	private String title;
	private int length;

	@Override
	public int compareTo(Track obj) {
		//SORT BY TITLE
		
		return this.getTitle().compareTo(obj.getTitle());
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERR: Track length is 0");
			throw(new PlayerException());
		}
		System.out.println("Playing Track: "+this.getTitle());
		System.out.println("Track Length : "+this.getLength());
	}

	public Track() {
	}
	

}
