package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media implements Comparable<Book> {

	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();

	@Override
	public int compareTo(Book obj) {
		// SORT BY COST

		return super.getCost() >= obj.getCost() ? 1 : -1;
	}

	@Override
	public String toString() {
		return "Book's name= " + this.getTitle() + "\tCategory= " + this.getCategory() + "\tCost= " + this.getCost()
				+ "\nNumber of Tokens: " + this.getWordFrequency().size() + ". Which are(Token = Frequency):\n" + this.wordFrequency.toString();
	}

	public void processContent() {
		String[] split1 = this.content.split("[\\.,:;!?()\\/\\=\\+\\-\\t\\r\\n]");

		for (int i = 0; i < split1.length; i++) {
			String[] split2 = split1[i].split(" ");
			for (int j = 0; j < split2.length; j++) {
				if (split2[j].isEmpty() == false)
					this.contentTokens.add(split2[j].toLowerCase());
				// ADD TO LIST TOKEN
			}
		}
		this.contentTokens.sort(null);

		// ADD TO TREE MAP
		for (int i = 0; i < this.contentTokens.size(); i++) {
			if (this.wordFrequency.containsKey(this.contentTokens.get(i)) == false)
				this.wordFrequency.put(this.contentTokens.get(i), 1);
			else
				this.wordFrequency.put(this.contentTokens.get(i),
						this.wordFrequency.get(this.contentTokens.get(i)) + 1);
		}
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public boolean addAuthor(String authorName) {
		if (this.authors.contains(authorName)) // author is already in the ArrayList before adding
			return false;
		else {
			this.authors.add(authorName);
			return true;
		}
	}

	public boolean removeAuthor(String authorName) {
		if (this.authors.contains(authorName) == false) // author is not in the ArrayList before remove
			return false;
		else {
			this.authors.remove(authorName);
			return true;
		}
	}

	public Book() {
	}

	public Book(String title) {
		super(title);
	}

	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		this.processContent();
	}

	public List<String> getContentTokens() {
		return contentTokens;
	}

	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}

}
