package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc> {
	private String director;
	private int length;

	@Override
	public int compareTo(DigitalVideoDisc obj) {
		// SORT BY COST

		return super.getCost() >= obj.getCost() ? 1 : -1;
	}

	@Override
	public String toString() {
		return "DVD's name= " + this.getTitle() + "\tCategory= " + this.getCategory() + "\tCost= " + this.getCost();
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERR: DVD length is 0");
			throw(new PlayerException());
		}
		System.out.println("Playing DVD: "+this.getTitle());
		System.out.println("DVD Length : "+this.getLength());
	}

	public DigitalVideoDisc() {
	}

	public DigitalVideoDisc(String title) {
		super.setTitle(title);
	}

	public DigitalVideoDisc(String title, String category) {
		super.setTitle(title);
		super.setCategory(category);
	}

	public DigitalVideoDisc(String title, String category, float cost) {
		super.setTitle(title);
		super.setCategory(category);
		super.setCost(cost);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super.setTitle(title);
		super.setCategory(category);
		this.director = director;
	}

}
