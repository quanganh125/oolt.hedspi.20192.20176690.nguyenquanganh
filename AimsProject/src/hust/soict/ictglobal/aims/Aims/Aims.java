package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {

	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("---------------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add DigitalVideoDisc to the order");
		System.out.println("3. Add CompactDisc to the order");
		System.out.println("4. Add Book to the order");
		System.out.println("5. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("---------------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4-5");
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int yourChoise = -1;
		String title, category, director, artist;
		float cost;
		int length;
		Order newOrder = null;

		showMenu();
		while (yourChoise != 0) {
			System.out.print("Your choise: ");
			yourChoise = Integer.parseInt(scan.nextLine());

			switch (yourChoise) {
			case 1:
				newOrder = new Order();
				newOrder.setDateOrdered();
				newOrder.updateNBOrder();
				System.out.println("Create new order succesfull ");
				System.out.println("=====================================");
				break;
			case 2:
				DigitalVideoDisc test = new DigitalVideoDisc();
				System.out.print("Disc's title: ");
				title = scan.nextLine();
				System.out.print("Disc's category: ");
				category = scan.nextLine();
				System.out.print("Disc's director: ");
				director = scan.nextLine();
				System.out.print("Disc's cost: ");
				cost = Float.parseFloat(scan.nextLine());
				System.out.print("Disc's length: ");
				length = Integer.parseInt(scan.nextLine());

				test.setTitle(title);
				test.setCategory(category);
				test.setCost(cost);
				test.setDirector(director);
				test.setLength(length);

				newOrder.addMedia(test);
				System.out.println("=====================================");
				break;
			case 3:
				CompactDisc test2 = new CompactDisc();
				System.out.print("CompactDisc's title: ");
				title = scan.nextLine();
				System.out.print("CompactDisc's category: ");
				category = scan.nextLine();
				System.out.print("CompactDisc's director: ");
				director = scan.nextLine();
				System.out.print("CompactDisc's cost: ");
				cost = Float.parseFloat(scan.nextLine());
				System.out.print("CompactDisc's length: ");
				length = Integer.parseInt(scan.nextLine());
				System.out.print("CompactDisc's artist: ");
				artist = scan.nextLine();

				test2.setTitle(title);
				test2.setCategory(category);
				test2.setCost(cost);
				test2.setLength(length);
				test2.setDirector(director);
				test2.setArtist(artist);

				newOrder.addMedia(test2);
				System.out.println("=====================================");
				break;
			case 4:
				Book test3 = new Book();
				int i, number;

				System.out.print("Book's title: ");
				title = scan.nextLine();
				System.out.print("Book's category: ");
				category = scan.nextLine();
				System.out.print("Book's cost: ");
				cost = Float.parseFloat(scan.nextLine());
				System.out.print("Book's number of author: ");
				number = Integer.parseInt(scan.nextLine());
				for (i = 0; i < number; i++)
				{
					System.out.print("Book's author"+i+": ");
					test3.addAuthor(scan.nextLine());
				}
				
				test3.setTitle(title);
				test3.setCategory(category);
				test3.setCost(cost);
				
				newOrder.addMedia(test3);
				System.out.println("=====================================");
				break;
			case 5:
				if (newOrder != null)
					newOrder.printOrder();
				break;
			case 0:
				System.out.println("Arigatou gozaimasu !!!");
				break;
			default:
				System.out.println("Incorrect Input. Try again! ");
				break;
			}
		}
		scan.close();
		
		/*List<DigitalVideoDisc> collection = new ArrayList<DigitalVideoDisc>();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("quang anh","disc1",20);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("hoa","disc2",22);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("quynh anh","disc3",15);
		collection.add(dvd1);
		collection.add(dvd2);
		collection.add(dvd3);
		
		Iterator<DigitalVideoDisc> iterator= collection.iterator(); 
		
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).toString());
		}
		System.out.println("================AFTER SORT================");
		Collections.sort(collection);
		
		iterator= collection.iterator(); 
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).toString());
		}*/
	}	
}
