package hust.soict.ictglobal.aims.order;

import java.util.ArrayList;
import java.util.List;

import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.date.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;

	private MyDate dateOrdered;
	private static int nbOrders = 0;
	private List<Media> itemsOrdered = new ArrayList<Media>();

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public boolean addMedia(Media media) {
		if (itemsOrdered.indexOf(media) != -1) {
			System.out.println("Media is already exist");
			return false; // Media is already exist
		} else if (this.checkMaxOrders()) {
			System.out.println("Max number of order");
			return false;
		} else {
			this.itemsOrdered.add(media);
			System.out.println("Add successfully");
			this.updateNBOrder();
			return true;
		}
	}

	public boolean removeMedia(Media media) {
		if (itemsOrdered.contains(media) == false) // Can't Find???
		{
			System.out.println("Can't find in the item orderes");
			return false;
		} else {
			itemsOrdered.remove(media);
			System.out.println("Remove successfully");
			return true;
		}
	}

	public void setDateOrdered(String dateOrdered) {
		MyDate tmp = new MyDate();
		tmp.accept(dateOrdered);
		this.dateOrdered = tmp;
	}

	public void setDateOrdered() {
		MyDate tmp = new MyDate();
		tmp.setCurrentDate();
		this.dateOrdered = tmp;
	}

	public int updateNBOrder() {
		return Order.nbOrders++;
	}

	public int getNBOrder() {
		return Order.nbOrders;
	}

	public boolean checkMaxOrders() {
		if (Order.nbOrders == MAX_LIMITTED_ORDERS)
			return true;
		return false;
	}

	public float totalCost() {
		int i;
		float total = 0;
		for (i = 0; i < this.itemsOrdered.size(); i++) {
			total += itemsOrdered.get(i).getCost();
		}
		return total;
	}

	public void printOrder() {
		int i;
		System.out.println("************************************************************");
		System.out.println("Date: " + this.getDateOrdered().getDate());
		System.out.println("Order Items:");
		for (i = 0; i < this.itemsOrdered.size(); i++) {
			System.out.println(i + 1 +". "+ itemsOrdered.get(i).toString());
		}
		System.out.println("Total cost: " + this.totalCost() + "$");
		System.out.println("************************************************************");
	}

	public List<Media> getItemsOrdered() {
		return itemsOrdered;
	}

	public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
		this.itemsOrdered = itemsOrdered;
	}
}
