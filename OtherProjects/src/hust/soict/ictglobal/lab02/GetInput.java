package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class GetInput {
	public static void main(String[] args) {
		Scanner people = new Scanner(System.in);

		System.out.println("What's your name? ");
		String strName = people.nextLine();
		System.out.println("How old are you? ");
		int iAge = people.nextInt();
		System.out.println("How tall are you? (m) ");
		double dHeigh = people.nextDouble();

		System.out.println("Mr/Mrs: " + strName + " " + iAge + " years old " + dHeigh + "m");
		people.close();
		System.exit(0);
	}
}
