package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class SortArray {
	public static void main(String[] args) {
		Scanner getInput = new Scanner(System.in);
		int number = 0, i,j,temp;
		
		System.out.println("Please enter number of elements: ");
		number = getInput.nextInt();
		int[] array = new int[number];
		for (i = 0; i < number; i++) {
			System.out.println("Please enter elements A[" + i + "]: ");
			array[i] = getInput.nextInt();
		}
		
		for (i=0;i<number;i++)
			for(j=0;j<i;j++)
			{
				if(array[j]> array[j+1]) {
					temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
			}
		for (i=0;i<number;i++) System.out.print(array[i]+",");
		getInput.close();
		System.exit(0);
	}
}
