package hust.soict.ictglobal.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class NoGarbage {
	public static void main(String[] args) {
		try {
			 File f = new File("H:/readme.txt");
		     FileReader fr = new FileReader(f);
	
		     BufferedReader br = new BufferedReader(fr);
		     String line;
		     while ((line = br.readLine()) != null){
		       System.out.println(line);
		     }
		     fr.close();
		     br.close();
		} catch (Exception ex) {
			System.out.println("Loi doc file: "+ex);
		}
	}
}
