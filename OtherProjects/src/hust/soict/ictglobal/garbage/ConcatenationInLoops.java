package hust.soict.ictglobal.garbage;

import java.util.Random;

public class ConcatenationInLoops {

	public static void main(String[] args) {
		Random rd = new Random(123);
		long start = System.currentTimeMillis();
		String s = "";
		// int i;
		for (int i = 0; i < 65536; i++)
			s += rd.nextInt(2);
		System.out.println(System.currentTimeMillis()-start);
		//***************************************************************
		System.out.println("|"+s+"|");
		start = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 65536; i++)
			sb.append(rd.nextInt(2));
		s= sb.toString();
		System.out.println(System.currentTimeMillis()-start);
		System.exit(0);
	}
}
